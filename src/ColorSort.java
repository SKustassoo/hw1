public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
	  // Leidsin et see on t��p�lesanne nimega Dutch national flag problem: https://en.wikipedia.org/wiki/Dutch_national_flag_problem
	  //Selle p�hjal hakkasin tegelema loogikaga
	  // Alguse ja l�pu asukohad on vaja, et liigutada vajadusel pallid kas �hele v�i teisele poole.
       int algus= 0;
       //l�pp on vajalik, et hakata sinistega teiselt poolt vastu tulema
       int l�pp = balls.length-1;
	   
	  //Kuidas loopida arrayd nii, et elementide kohta saab vahetada : http://stackoverflow.com/questions/7970857/java-shifting-elements-in-an-array
	   //Selle jaoks,  et hoida m�lus ajutiselt vaja minevat v�rvi v��rtust
       
	   
	   //Iga elementi kohta balls arrays teeb �he korra
	   for(int i = 0; i<=l�pp; i++){
		   if(algus < l�pp){
		   //if element kohal i on punane (enum)
		   if(balls[i] == Color.red){
			   //paneb balls[i] v��rtuse temp sisse
		       Color temp = balls[i];
		       //balls[i] asukoht muudetakse esimeseks vaba olevaks elemendiks
		       balls[i] = balls[algus];
		       //uuel asukohal olevale elemendile antakse v��rtus temp seest
		       balls[algus] = temp;
		       algus++;
	       //if element kohal i on sinine
		   /* Kui on balls[i] on sinine siis teeb sama moodi nagu punasel
		    * aga paneb viimasele kohale arrays
		    * ning teeb i �he v�rra v�iksemaks, mis vajalik sest for loop teeb selle j�lle suuremaks aj sisi kontrollib sama koha pealt elementi, kus ennem. (et ei j�taks vahale �htegi)
		    * */
		   }else if(balls[i] == Color.blue){
		       Color temp = balls[i];
		       balls[i] = balls[l�pp];
		       balls[l�pp] = temp;
		       l�pp--;
		       i--;
		   }
		   }
	   }
   }
}

